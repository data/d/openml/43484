# OpenML dataset: Bus-Breakdown-and-Delays-NYC

https://www.openml.org/d/43484

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Bus Breakdown and Delays
You can find the road where the traffic was heavy for the New York City Taxi Trip Duration playground. 
Content
The Bus Breakdown and Delay system collects information from school bus vendors operating out in the field in real time. Bus staff that encounter delays during the route are instructed to radio the dispatcher at the bus vendors central office. The bus vendor staff are then instructed to log into the Bus Breakdown and Delay system to record the event and notify OPT. OPT customer service agents use this system to inform parents who call with questions regarding bus service. The Bus Breakdown and Delay system is publicly accessible and contains real time updates. All information in the system is entered by school bus vendor staff.
You can find data for years 2015 to 2017.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43484) of an [OpenML dataset](https://www.openml.org/d/43484). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43484/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43484/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43484/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

